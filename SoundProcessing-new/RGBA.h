#pragma once
#if !defined(INC_RGBA_H)
#define INC_RGBA_H

//=============================================================

#include <iosfwd>

//=============================================================
class RGBA
{
public:
    RGBA();
    RGBA(int red, int green, int blue, int alpha = 255);

    bool hasAlpha() const { return alpha != 255; }

public:
    int red;
    int green;
    int blue;
    int alpha;
};

//=============================================================
std::istream& operator>>(std::istream& stream, RGBA& rgba);

//=============================================================

#endif // #if !defined(INC_RGBA_H)