#if !defined(INC_AUDIO_PROCESSOR_H)
#define INC_AUDIO_PROCESSOR_H
//=============================================================
class AudioProcessor
{
public:
    //=============================================================
    virtual ~AudioProcessor();
    //=============================================================
    virtual bool init
    (
        int sample_rate,
        int channels,
        long frame_count,
        int buffer_size
    ) = 0;

    virtual bool ContinueOrNot() const = 0;
    virtual bool process
    (
        const short* input_buffer,
        int input_frame_count
    ) = 0;
    virtual void done() = 0;
};
//=============================================================

#endif // #if !defined(INC_AUDIO_PROCESSOR_H)