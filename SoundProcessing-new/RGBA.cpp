#include "Rgba.h"
#include<regex>
#include <iostream>
#include<string>
//=============================================================

RGBA::RGBA() :
    red(0),
    green(0),
    blue(0),
    alpha(255) // fully opaque
{

}

RGBA::RGBA(int r, int g, int b, int a) :
    red(r),
    green(g),
    blue(b),
    alpha(a)
{

}

//=============================================================

static int parseHex(const std::string& str)
{
    return static_cast<int>(strtoul(str.c_str(), nullptr, 16));
}

//=============================================================

std::istream& operator>>(std::istream& stream, RGBA& rgba)
{
    std::string value;
    stream >> value;
    std::regex r("^([0-9A-Fa-f]{2})([0-9A-Fa-f]{2})([0-9A-Fa-f]{2})([0-9A-Fa-f]{2})?$");
    std::smatch match;
    if (std::regex_match(value,match,r)) {
        rgba.red = parseHex(match[1].str());
        rgba.green = parseHex(match[2].str());
        rgba.blue = parseHex(match[3].str());

        if (match[4].matched)
        {
            rgba.alpha = parseHex(match[4].str());
        }
    }

    else
    {
        throw("Invalid color value");
    }

    return stream;
}

//=============================================================