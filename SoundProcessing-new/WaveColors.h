#pragma once
#if !defined(INC_WAVEFORM_COLORS_H)
#define INC_WAVEFORM_COLORS_H

//=============================================================

#include "Rgba.h"

//=============================================================

class WaveColors
{
public:
    WaveColors();
    WaveColors(
        const RGBA& border_color,
        const RGBA& background_color,
        const RGBA& wave_color,
        const RGBA& axis_label_color
    );

    bool hasAlpha() const;

public:
    RGBA border_color;
    RGBA background_color;
    RGBA waveform_color;
    RGBA axis_label_color;
};

//=============================================================

extern const WaveColors audacity_waveform_colors;
extern const WaveColors audition_waveform_colors;

//=============================================================

#endif // #if !defined(INC_WAVEFORM_COLORS_H)
