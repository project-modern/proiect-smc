#include "Math.h"
#include<regex>
//=============================================================

namespace Math
{
    //=============================================================

    // Rounds the given value down to the nearest given multiple.
    int roundDownToNearest(double value, int multiple)
    {
        if (multiple == 0) {
            return 0;
        }

        return multiple * (static_cast<int>(value) / multiple);
    }

    // Rounds the given value up to the nearest given multiple.
    int roundUpToNearest(double value, int multiple)
    {
        if (multiple == 0) {
            return 0;
        }

        int multiplier = 1;

        if (value < 0.0) {
            multiplier = -1;
            value = -value;
        }

        const int rounded_up = static_cast<int>(ceil(value));

        return multiplier * ((rounded_up + multiple - 1) / multiple) * multiple;
    }

    //=============================================================

    std::pair<bool, double> parseNumber(const std::string& value)
    {
       static const  std::regex regex(
            "^"          // Anchor to beginning of string
            "[\\+\\-]?"  // Optionally, either a '+' or a '-'
            "\\d*"       // Zero or more digits
            "(\\.\\d*)?" // Optionally, a '.' followed by zero or more digits
            "$"          // Anchor to end of string
        );

        double number = 0.0;
        bool success;
        std::smatch match;
        if (std::regex_match(value, match, regex))
        {
            try
            {
                number = std::stod(value);
                success = true;
            }

            catch (const std::exception& e)
            {
                success = false;
            }
        }

        else
        {
            success = false;
        }

        return std::make_pair(success, number);
    }

    //=============================================================

    // Multiply value by amplitude_scale, but limit the output to -32768 to 32767.

    short scale(int value, double multiplier)
    {
        double result = value * multiplier;

        if (result > 32767.0) {
            result = 32767.0;
        }

        else if (result < -32768.0) {
            result = -32768.0;
        }

        return static_cast<short>(result);
    }

    //=============================================================

} 
