#pragma once
#if !defined(INC_MATH_UTIL_H)
#define INC_MATH_UTIL_H
#include <string>
#include <utility>

//=============================================================

namespace Math 
{
    //=============================================================
    int roundDownToNearest(double value, int multiple);
    int roundUpToNearest(double value, int multiple);
    //=============================================================
    std::pair<bool, double> parseNumber(const std::string& value);
    //=============================================================
    short scale(int value, double multiplier);
    //=============================================================
}

//=============================================================

#endif // #if !defined(INC_MATH_UTIL_H)