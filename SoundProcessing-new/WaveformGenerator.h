#if !defined(INC_WAVEFORM_GENERATOR_H)
#define INC_WAVEFORM_GENERATOR_H

//=============================================================
#include "AudioProcessor.h"

#include <vector>

//=============================================================

class WaveformBuffer;

//=============================================================

class ScaleFactor
{
public:
    virtual ~ScaleFactor();

public:
    virtual int getSamplesPerPixel(int sample_rate) const = 0;
};

//=============================================================

class SamplesPerPixelScaleFactor : public ScaleFactor
{
public:
    SamplesPerPixelScaleFactor(int samples_per_pixel);

public:
    virtual int getSamplesPerPixel(int sample_rate) const;

private:
    int samples_per_pixel_;
};
//=============================================================

class PixelsPerSecondScaleFactor : public ScaleFactor
{
public:
    PixelsPerSecondScaleFactor(int pixels_per_second);

public:
    virtual int getSamplesPerPixel(int sample_rate) const;

private:
    int pixels_per_second_;
};

//=============================================================

class DurationScaleFactor : public ScaleFactor
{
public:
    DurationScaleFactor(
        double start_time,
        double end_time,
        int width_pixels
    );

public:
    virtual int getSamplesPerPixel(int sample_rate) const;

private:
    double start_time_;
    double end_time_;
    int width_pixels_;
};

//=============================================================

class WaveformGenerator : public AudioProcessor
{
public:
    WaveformGenerator(
        WaveformBuffer& buffer,
        bool split_channels,
        const ScaleFactor& scale_factor
    );

    WaveformGenerator(const WaveformGenerator&) = delete;
    WaveformGenerator& operator=(const WaveformGenerator&) = delete;

public:
    virtual bool init
    (
        int sample_rate,
        int channels,
        long frame_count,
        int buffer_size
    );

    virtual bool ContinueOrNot() const;

    int getSamplesPerPixel() const;

    virtual bool process(
        const short* input_buffer,
        int input_frame_count
    );

    virtual void done();

private:
    void reset();

private:
    WaveformBuffer& buffer_;
    const ScaleFactor& scale_factor_;
    bool split_channels_;
    int channels_;
    int output_channels_;
    int samples_per_pixel_;
    int count_;
    std::vector<int> min_;
    std::vector<int> max_;
};

//=============================================================

#endif // #if !defined(INC_WAVEFORM_GENERATOR_H)

//=============================================================