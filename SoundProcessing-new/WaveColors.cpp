#include "WaveColors.h"
#include "Rgba.h"

//=============================================================

WaveColors::WaveColors()
{

}

WaveColors::WaveColors(
    const RGBA& border,
    const RGBA& background,
    const RGBA& waveform,
    const RGBA& axis_label) :
    border_color(border),
    background_color(background),
    waveform_color(waveform),
    axis_label_color(axis_label)
{
}

//=============================================================

bool WaveColors::hasAlpha() const
{
    return border_color.hasAlpha() ||
        background_color.hasAlpha() ||
        waveform_color.hasAlpha() ||
        axis_label_color.hasAlpha();
}

//=============================================================

const WaveColors audacity_waveform_colors(
    { 0, 0, 0 },
    { 214, 214, 214 },
    { 63, 77, 155 },
    { 0, 0, 0 }
);


const WaveColors audition_waveform_colors(
    { 157, 157, 157 },
    { 0, 63, 34 },
    { 134, 252, 199 },
    { 190, 190, 190 }
);

//=============================================================