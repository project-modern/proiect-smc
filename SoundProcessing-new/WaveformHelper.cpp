#include "WaveformHelper.h"
#include "Math.h"
#include "WaveformBuffer.h"
#include <cassert>
#include <cmath>
#include <limits>



namespace WaveformHelper 
{
    // Returns the minimum and maximum values over a given region of the buffer.
    static std::pair<int, int> getAmplitudeRange
    (
        const WaveformBuffer& buffer,
        int start_index,
        int end_index)
    {
        int low = std::numeric_limits<int>::max();
        int high = std::numeric_limits<int>::min();
        const int channels = buffer.getChannels();
        for (int i = start_index; i != end_index; ++i)
        {
            for (int channel = 0; channel < channels; ++channel)
            {
                const int min = buffer.getMinSample(channel, i);
                const int max = buffer.getMaxSample(channel, i);

                if (min < low)
                {
                    low = min;
                }

                if (max > high)
                {
                    high = max;
                }
            }
        }

        return std::make_pair(low, high);
    }

    //=============================================================

    double getAmplitudeScale(
        const WaveformBuffer& buffer,
        int start_index,
        int end_index)
    {
        assert(start_index >= 0);
        assert(start_index <= buffer.getSize());

        assert(end_index >= 0);
        assert(end_index <= buffer.getSize());

        assert(end_index > start_index);

        const std::pair<int, int> range = getAmplitudeRange(buffer, start_index, end_index);

        const double amplitude_scale_high = (range.second == 0) ? 1.0 : 32767.0 / range.second;
        const double amplitude_scale_low = (range.first == 0) ? 1.0 : 32767.0 / range.first;

        return std::fabs(std::min(amplitude_scale_high, amplitude_scale_low));
    }

    //=============================================================

    void scaleWaveformAmplitude(WaveformBuffer& buffer, double amplitude_scale)
    {
        const int size = buffer.getSize();
        const int channels = buffer.getChannels();

        for (int i = 0; i != size; ++i)
        {
            for (int channel = 0; channel < channels; ++channel)
            {
                const short min = Math::scale
                (
                    buffer.getMinSample(channel, i),
                    amplitude_scale
                );

                const short max = Math::scale
                (
                    buffer.getMaxSample(channel, i),
                    amplitude_scale
                );

                buffer.setSamples(channel, i, min, max);
            }
        }
    }

    //=============================================================

}