#pragma once
#if !defined(INC_WAVEFORM_UTIL_H)
#define INC_WAVEFORM_UTIL_H
class WaveformBuffer;
//=============================================================

namespace WaveformHelper
{
    double getAmplitudeScale
    (
        const WaveformBuffer& buffer,
        int start_index,
        int end_index
    );

    void scaleWaveformAmplitude
    (
        WaveformBuffer& buffer,
        double amplitude_scale
    );
}

//=============================================================

#endif // #if !defined(INC_WAVEFORM_UTIL_H)
