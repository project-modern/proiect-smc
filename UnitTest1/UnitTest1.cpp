#include "pch.h"
#include "CppUnitTest.h"
#include "../SoundProcessing-new/Operations.h"
#include "../SoundProcessing-new/Operations.cpp"
#include "../SoundProcessing-new/Audio.h"
#include "../SoundProcessing-new/Audio.cpp"
#include "../SoundProcessing-new/Commands.h"
#include "../SoundProcessing-new/Commands.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(PlaybackTest)
		{
			std::vector<Audio> Playlist, modifiedSong;
			AudioFile<double> audio;
			audio.load("C:/Users/Daniel/Desktop/Final/guitar.wav");
			double SampleRate = audio.getSampleRate();
			std::pair<AudioFile<double>, std::string> per;
			per.first = audio;
			per.second = "C:/Users/Daniel/Desktop/Final/guitar.wav";
			Audio toSave(per);
			Playlist.push_back(toSave);
			std::string command = "--playback fast 3 No Complaints.wav PlaybackTest.wav";
			Operations op(command);
			op.GeneralizedPlayback(Playlist, modifiedSong, "No Complaints.wav", "PlaybackTest.wav", "fast", 3, 0);


			uint32_t newValue = SampleRate + (0.1 * 3 * SampleRate);
			Assert::AreEqual(modifiedSong[0].GetAudio().getSampleRate(), newValue);
		}


		TEST_METHOD(ScaleTest)
		{
			std::vector<Audio> Playlist, modifiedSong;
			AudioFile<double> audio;
			audio.load("C:/Users/Daniel/Desktop/Final/guitar.wav");
			double SampleRate = audio.getSampleRate();
			std::pair<AudioFile<double>, std::string> per;
			per.first = audio;
			per.second = "C:/Users/Daniel/Desktop/Final/guitar.wav";
			Audio toSave(per);
			Playlist.push_back(toSave);
			std::string command = "--scale up 3 No Complaints.wav ScaleTest.wav";
			Operations op(command);
			op.GeneralizedScale(Playlist, modifiedSong, "No Complaints.wav", "ScaleTest.wav", "up", 3, 0);




			double newValue;
			for (int canal = 0; canal < audio.getNumChannels(); canal++)
			{
				for (int i = 0; i < audio.getNumSamplesPerChannel(); i++)
				{
					newValue = audio.samples[canal][i] + ((double)3 / 10 * audio.samples[canal][i]);
					Assert::AreEqual(newValue, modifiedSong[0].GetAudio().samples[canal][i]);
				}
			}

		}


		TEST_METHOD(ArtificialSignalsTest)
		{
			std::vector<Audio> Playlist;
			std::string command = "artificial.wav 20 340 44100 C:/Users/Andy/Desktop/Test";
			Operations op(command);
			op.ArtificialSignals();
			AudioFile<double> audio;
			audio.load("C:/Users/Andy/Desktop/Test/artificial.wav");
			double sample1 = audio.getSampleRate();
			double sample2 = 44100;
			//double length1 = audio.getLengthInSeconds();
			//double length2 = 46000 * 20;
			Assert::AreEqual(sample1, sample2);
			//Assert::AreEqual(length1, length2);


		}

		TEST_METHOD(LoadTest)
		{
			std::vector<Audio> Playlist;
			std::string command = "--load C:/Users/Andy/Desktop/Test/song.wav";
			Operations op(command);
			op.Load(Playlist);
			bool verif = false;
			Assert::AreEqual(verif, Playlist.empty());
		}

		TEST_METHOD(RemoveSongTest)
		{
			std::vector<Audio> Playlist, ModifiedPlaylist;
			std::string command = "--load C:/Users/Andy/Desktop/Test/song.wav";
			Operations op(command);
			op.Load(Playlist);
			command = "--removeAudio song.wav";
			Operations op2(command);
			op2.RemoveSong(Playlist, ModifiedPlaylist);
			bool verif = true;
			Assert::AreEqual(verif, Playlist.empty());
			
		}
	};
}
